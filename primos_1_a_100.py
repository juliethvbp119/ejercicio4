numeros=list(range(1, 101))
numerosPrimos=[]

def es_prim(numero):
    if numero >1:
        for n in range(2,numero):
            if numero % n == 0:
                return False
        return True
        
for item in numeros:
    if es_prim(item):
        numerosPrimos.append(item)
    
print("numeros primos es: "+ str(numerosPrimos))
